 terraform {
 backend "s3" {
    bucket = "tfstate-tom2"
    key    = "terraform-panos.tfstate"
    region = "eu-west-2"
  }
}

data "terraform_remote_state" "core" {
  backend = "s3"

  config = {
    region  = "eu-west-2"
    bucket  = "tfstate-tom2"
    key     = "terraform.tfstate"
  }
}
